import * as React from 'react'
import API from '../libs/API'

import '../style/general.scss'

import Layout from '../components/Layout/Layout'
import Header from '../components/Header/Header'
import ImageGrid from '../components/ImageGrid/ImageGrid'
import Section from '../components/UI/Section/Section'
import Headline from '../components/UI/Headline/Headline'
import Button from '../components/UI/Button/Button'
import Text from '../components/UI/Text/Text'
import TextSection from '../components/Sections/TextSection/TextSection'
import BlogPostPreview from '../components/Blog/BlogPostPreview/BlogPostPreview'
import FlexGrid from '../components/UI/FlexGrid/FlexGrid'

const IMG_FOLDER = '/static/images/mecenes'

const posts: PostPreview[] = [
  {
    id: '1',
    title: 'Un super beau titre de la mort',
    date: new Date(),
    excerpt:
      'lorem emdvkje fvsjfn vlskjfnv lzjkfnvlz rjvnzl rvnls fjknvz ljkfnvz lnvz lfjnvz ljfnv zlrnv zlrjknv zlkjrvnzlktrhvzkjrhzl rjkvzn rlgkjbnz rlgjknv lkjrnvz lkjrfnv ljvz',
    slug: 'un-super-title',
    imageURL:
      'http://ales-mecenat.fr/wp-content/uploads/2019/03/54413263_1920951694699314_4453338897870487552_o-848x450.jpg'
  },
  {
    id: '2',
    title: 'Un super beau titre de la mort',
    date: new Date(),
    excerpt:
      'lorem emdvkje fvsjfn vlskjfnv lzjkfnvlz rjvnzl rvnls fjknvz ljkfnvz lnvz lfjnvz ljfnv zlrnv zlrjknv zlkjrvnzlktrhvzkjrhzl rjkvzn rlgkjbnz rlgjknv lkjrnvz lkjrfnv ljvz',
    slug: 'un-super-title',
    imageURL:
      'http://ales-mecenat.fr/wp-content/uploads/2019/03/54413263_1920951694699314_4453338897870487552_o-848x450.jpg'
  },
  {
    id: '3',
    title: 'Un super beau titre de la mort',
    date: new Date(),
    excerpt:
      'lorem emdvkje fvsjfn vlskjfnv lzjkfnvlz rjvnzl rvnls fjknvz ljkfnvz lnvz lfjnvz ljfnv zlrnv zlrjknv zlkjrvnzlktrhvzkjrhzl rjkvzn rlgkjbnz rlgjknv lkjrnvz lkjrfnv ljvz',
    slug: 'un-super-title',
    imageURL:
      'http://ales-mecenat.fr/wp-content/uploads/2019/03/54413263_1920951694699314_4453338897870487552_o-848x450.jpg'
  },
  {
    id: '4',
    title: 'Un super beau titre de la mort',
    date: new Date(),
    excerpt:
      'lorem emdvkje fvsjfn vlskjfnv lzjkfnvlz rjvnzl rvnls fjknvz ljkfnvz lnvz lfjnvz ljfnv zlrnv zlrjknv zlkjrvnzlktrhvzkjrhzl rjkvzn rlgkjbnz rlgjknv lkjrnvz lkjrfnv ljvz',
    slug: 'un-super-title',
    imageURL:
      'http://ales-mecenat.fr/wp-content/uploads/2019/03/54413263_1920951694699314_4453338897870487552_o-848x450.jpg'
  },
  {
    id: '5',
    title: 'Un super beau titre de la mort',
    date: new Date(),
    excerpt:
      'lorem emdvkje fvsjfn vlskjfnv lzjkfnvlz rjvnzl rvnls fjknvz ljkfnvz lnvz lfjnvz ljfnv zlrnv zlrjknv zlkjrvnzlktrhvzkjrhzl rjkvzn rlgkjbnz rlgjknv lkjrnvz lkjrfnv ljvz',
    slug: 'un-super-title',
    imageURL:
      'http://ales-mecenat.fr/wp-content/uploads/2019/03/54413263_1920951694699314_4453338897870487552_o-848x450.jpg'
  },
  {
    id: '6',
    title: 'Un super beau titre de la mort',
    date: new Date(),
    excerpt:
      'lorem emdvkje fvsjfn vlskjfnv lzjkfnvlz rjvnzl rvnls fjknvz ljkfnvz lnvz lfjnvz ljfnv zlrnv zlrjknv zlkjrvnzlktrhvzkjrhzl rjkvzn rlgkjbnz rlgjknv lkjrnvz lkjrfnv ljvz',
    slug: 'un-super-title',
    imageURL:
      'http://ales-mecenat.fr/wp-content/uploads/2019/03/54413263_1920951694699314_4453338897870487552_o-848x450.jpg'
  },
  {
    id: '7',
    title: 'Un super beau titre de la mort',
    date: new Date(),
    excerpt:
      'lorem emdvkje fvsjfn vlskjfnv lzjkfnvlz rjvnzl rvnls fjknvz ljkfnvz lnvz lfjnvz ljfnv zlrnv zlrjknv zlkjrvnzlktrhvzkjrhzl rjkvzn rlgkjbnz rlgjknv lkjrnvz lkjrfnv ljvz',
    slug: 'un-super-title',
    imageURL:
      'http://ales-mecenat.fr/wp-content/uploads/2019/03/54413263_1920951694699314_4453338897870487552_o-848x450.jpg'
  },
  {
    id: '8',
    title: 'Un super beau titre de la mort',
    date: new Date(),
    excerpt:
      'lorem emdvkje fvsjfn vlskjfnv lzjkfnvlz rjvnzl rvnls fjknvz ljkfnvz lnvz lfjnvz ljfnv zlrnv zlrjknv zlkjrvnzlktrhvzkjrhzl rjkvzn rlgkjbnz rlgjknv lkjrnvz lkjrfnv ljvz',
    slug: 'un-super-title',
    imageURL:
      'http://ales-mecenat.fr/wp-content/uploads/2019/03/54413263_1920951694699314_4453338897870487552_o-848x450.jpg'
  }
]

const BlogPage = ({ posts }: { posts: WP_POST[] }) => {
  const [featured1, featured2, ...otherPosts] = posts
  return (
    <Layout>
      <Header />
      <Section
        light={false}
        spaced
        style={{ paddingTop: '160px' }}
        wave='/static/images/wave_top_grey.svg'
      >
        <>
          <Headline tag={'h1'}>Blog</Headline>
          {posts && posts.length && (
            <FlexGrid size={2}>
              {[featured1, featured2].map((post: WP_POST, index: number) => {
                console.log(post)
                return (
                  <BlogPostPreview
                    key={post.id}
                    title={post.title.rendered}
                    excerpt={post.excerpt.rendered}
                    imageURL={getImageUrl(
                      post._embedded['wp:featuredmedia'][0],
                      'full'
                    )}
                    size='S'
                    date={new Date(post.date)}
                    slug={post.slug}
                    id={'' + post.id}
                    {...{ reversed: index % 2 !== 0 }}
                    noShadow
                  />
                )
              })}
            </FlexGrid>
          )}
        </>
      </Section>
      <Section
        light={true}
        spaced
        style={{ paddingTop: '160px', background: '#ededed' }}
      >
        <FlexGrid size={4}>
          {otherPosts.map((post: WP_POST, index: number) => {
            return (
              <BlogPostPreview
                key={post.id}
                title={post.title.rendered}
                excerpt={post.excerpt.rendered}
                imageURL={getImageUrl(
                  post._embedded['wp:featuredmedia'][0],
                  'medium'
                )}
                size='S'
                date={new Date(post.date)}
                slug={post.slug}
                id={'' + post.id}
              />
            )
          })}
        </FlexGrid>
      </Section>
    </Layout>
  )
}

function getImageUrl (
  item: any,
  size: 'thumbnail' | 'medium_large' | 'medium' | 'full'
) {
  console.log({ item })
  if (
    item &&
    item.media_details &&
    item.media_details.sizes &&
    item.media_details.sizes[size]
  ) {
    return item.media_details.sizes[size].source_url
  }
  return '#'
}
export async function getServerSideProps () {
  // Fetch data from external API

  const data = await API.getPosts(10)

  console.log({ data })

  // Pass data to the page via props
  return { props: { posts: data } }
}

export default BlogPage
