import * as React from 'react'

import '../style/general.scss'

import Layout from '../components/Layout/Layout'
import Header from '../components/Header/Header'
import HeroSection from '../components/Sections/Hero/HeroSection'
import ServicesSection from '../components/Sections/Services/Services'
import MecenesSection from '../components/Sections/Mecenes/Mecenes'
import ContactSection from '../components/Sections/Contact/ContactSection'

const IndexPage = () => (
  <Layout>
    <Header />
    <HeroSection />
    <ServicesSection />
    <MecenesSection spaced />
    <ContactSection spaced />
  </Layout>
)

export default IndexPage
