import * as React from 'react'

import '../style/general.scss'

import Layout from '../components/Layout/Layout'
import Header from '../components/Header/Header'
import ImageGrid from '../components/ImageGrid/ImageGrid'
import Section from '../components/UI/Section/Section'
import Headline from '../components/UI/Headline/Headline'
import Button from '../components/UI/Button/Button'
import Text from '../components/UI/Text/Text'
import TextSection from '../components/Sections/TextSection/TextSection'

const IMG_FOLDER = '/static/images/mecenes'

const data = [
  {
    name: 'MSL',
    url: 'http://www.msl-stores.com/',
    src: `${IMG_FOLDER}/msl.png`
  },
  {
    name: 'Hurricane',
    url: 'http://www.hurricane.fr/',
    src: `${IMG_FOLDER}/hurricane.png`
  },
  {
    name: 'Veolia',
    url: 'https://www.service.eau.veolia.fr/home.html',
    src: `${IMG_FOLDER}/veolia.png`
  },
  {
    name: 'NTN SNR',
    url: 'https://www.ntn-snr.com/fr',
    src: `${IMG_FOLDER}/snr.png`
  },
  {
    name: 'Ozil Conseil',
    url: 'http://ozil-conseil.com',
    src: `${IMG_FOLDER}/ozilconseil.png`
  },
  {
    name: 'Mie Câline',
    url: 'https://www.lamiecaline.com/fr/',
    src: `${IMG_FOLDER}/la-mie-caline.png`
  },
  {
    name: 'B1ChezSoi',
    url: 'http://b1chezsoi.com/',
    src: `${IMG_FOLDER}/b1chezsoi.png`
  },
  {
    name: 'Orange',
    url: 'https://www.orange.fr/portail',
    src: `${IMG_FOLDER}/orange.png`
  },
  {
    name: 'Cereg',
    url: 'https://cereg.com/',
    src: `${IMG_FOLDER}/cereg.png`
  },
  {
    name: "Caisse d'Epargne",
    url: 'https://www.caisse-epargne.fr',
    src: `${IMG_FOLDER}/caisse-epargne.png`
  },
  {
    name: 'Enedis',
    url: 'https://www.enedis.fr/',
    src: `${IMG_FOLDER}/enedis.png`
  },
  {
    name: 'SDTech',
    url: 'https://groupe.sd-tech.com/',
    src: `${IMG_FOLDER}/sdtech.png`
  },
  {
    name: 'Cévennes Audition',
    url:
      'https://www.ales-commerces.com/commerces/optique-photo-audition/cevennes-audition-451.html',
    src: `${IMG_FOLDER}/cevennes-audition.png`
  },
  {
    name: 'Edipoles',
    url: 'https://www.edipoles.com/',
    src: `${IMG_FOLDER}/edipoles.png`
  },
  {
    name: 'Point S',
    url: 'https://www.points.fr/',
    src: `${IMG_FOLDER}/points.png`
  }
]

const CulturePage = () => (
  <Layout>
    <Header />
    <Section
      light={false}
      spaced
      style={{ paddingTop: '160px' }}
      wave='/static/images/wave_top.svg'
    >
      <>
        <Headline tag={'h1'}>Comité Culture</Headline>
        <ImageGrid items={data} />
      </>
    </Section>
    <TextSection
      title='Un petit mot du comité culture'
      text='Lorem ipsum dolor sit amet consectetur, adipisicing elit. Qui aspernatur magni deleniti ullam dicta, porro itaque delectus in recusandae fugit explicabo, veniam, placeat animi eaque esse fugiat cumque et laboriosam?'
      button={{ label: 'Contacter le comité', href: '#' }}
    />
  </Layout>
)

export default CulturePage
