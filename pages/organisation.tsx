import * as React from 'react'

import '../style/general.scss'

import Layout from '../components/Layout/Layout'
import Header from '../components/Header/Header'
import OrganisationSection from '../components/Sections/Organisation/OrganisationSection'
import PlanningSection from '../components/Sections/Planning/PlanningSection'

const OrganisationPage = () => (
  <Layout>
    <Header />
    <OrganisationSection />
    <PlanningSection />
  </Layout>
)

export default OrganisationPage
