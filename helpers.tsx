import { NextRouter } from 'next/router'

import Router from 'next/router'

export const navigate = (path = '/') => {
  Router.push(path).then(() => window.scrollTo(0, 0))
}

export const createNavigate = (router: NextRouter) => {
  return (path: string) => {
    router.push(path).then(() => window.scrollTo(0, 0))
  }
}
