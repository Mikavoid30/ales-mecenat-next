import { SUBMIT_FORM } from '../actions/types'

export default (state = null, action) => {
  switch (action.type) {
    case SUBMIT_FORM:
      return action.payload
    default:
      return state
  }
}
