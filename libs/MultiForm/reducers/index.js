import { combineReducers } from 'redux'
import { reducer as reduxForm } from 'redux-form'
import serverReducer from './serverReducer'

export default combineReducers({
  form: reduxForm,
  server: serverReducer
})