import React, { Component } from 'react'

import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import reduxThunk from 'redux-thunk'
import reducers from '../reducers'

const store = createStore(reducers, {}, applyMiddleware(reduxThunk))

import Steps from './Steps'

class App extends Component {
  render () {
    return (
      <Provider store={store}>
        <div className='app container'>
          <Steps />
        </div>
      </Provider>
    )
  }
}

export default App
