// Shows every pages
import React, { Component } from 'react'
import { reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import * as actions from '../actions'
import StepsPage from './StepsPage'
import steps, { getStep } from '../data/steps'

class Steps extends Component
{
  constructor(props) {
    super(props)
    this.state = {currentPageNumber: 0, loading: false}
    this.nextPage = this.nextPage.bind(this)
    this.previousPage = this.previousPage.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  componentDidMount() {
    this.setState({loading: false})
  }

  onSubmit(values) {
    this.setState({loading: true})
    this.props.submitForm(values, steps)
  }

  getPageInfos() {
    if (!steps[this.state.currentPageNumber]) return null
    return getStep(this.state.currentPageNumber, this.props.form.dossier && this.props.form.dossier.values ? this.props.form.dossier.values : [] )
  }

  nextPage() {
    return this.setState({currentPageNumber: this.state.currentPageNumber + 1})
  }

  previousPage(e) {
    e && e.preventDefault()
    if (this.state.currentPageNumber <= 0) return
    return this.setState({currentPageNumber: this.state.currentPageNumber - 1})
  }

  introMessage() {
    return (
      <div className="bg-info" style={{paddingTop: '30px'}}>
      <b>Vous vous apprêtez à faire un dépôt de dossier auprès d'Alès Mécénat.</b>
      <br />
      <p>Avant de lancer le formulaire veuillez prendre connaissances des conditions ci-dessous : </p>
      <ul style={{paddingLeft: '45px'}}>
          <li>Votre structure doit être domiciliée dans une commune du "Pays Cévennes"</li>
          <li>Votre structure doit être reconnue d'intêret général</li>
          <li>
          Votre projet doit présenter un caractère innovant et participer au rayonnement du territoire. Le comité de sélection Culture se basera sur ces critères pour évaluer son éligibilité. A défaut, il ne pourra être retenu.
          </li>
          <li>
            La rénovation du patrimoine bâti n’est pas éligible au fonds de dotation.
          </li>
          <li>Vous devez préparer un budget suivant le modèle à <a href="http://ales-mecenat.fr/wp-content/uploads/2016/03/Trame_Budget_previsionnel.xlsx" target="_blank">télécharger ici</a>. Il vous sera demandé dans la dernière étape du formulaire</li>
          <li>Vous devez préparer votre <a href="http://ales-mecenat.fr/la-procedure-de-rescrit-fiscal/" target="_blank">rescrit fiscal</a> et la copie de vos statuts au format pdf, doc ou xls, ils vous seront également demandés lors de la dernière étape du formulaire.</li>
      </ul>
      <p>
          Merci de votre compréhension, si vous êtes prêt, vous pouvez dès maintenant commencer la saisie de votre dossier.
      </p>
      <p>
        <strong>Lien utile: </strong><a href="http://ales-mecenat.fr/la-procedure-de-rescrit-fiscal/" target="_blank">La procédure de rescrit fiscal</a>
      </p>
      <div style={{ marginBottom: '15px' }}>
        <table className="striped">
        <thead>
          <tr>
              <th>Date limite de dépôt de dossier</th>
              <th>Réunion Comité Culture et Sport </th>
              <th>Réponse aux porteurs de projet</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>15 mars</td>
            <td>1ère quinzaine d'avril</td>
            <td>30 avril</td>
          </tr>
          <tr>
            <td>15 octobre</td>
            <td>1ère quinzaine de novembre</td>
            <td>30 novembre</td>
          </tr>
        </tbody>
      </table>
      </div>
    </div>
    )
  }

  render() {
    if (this.props.server && this.props.server.status === 204) {
      return (<p>Merci, votre demande sera traitée dans les plus brefs délais.</p>)
    }
    if (this.props.server && this.props.server.status === 422) {
      return (<p>Une erreur est survenue, merci de renouveler votre demande ultérieurement. Merci.</p>)
    }
    if (!this.props.server && this.state.loading) {
      return (
        <div>
          <p>Envoi de votre dossier ainsi que tous les fichiers vers le serveur. Celà peut prendre du temps.</p>
          <div className="progress">
              <div className="indeterminate"></div>
          </div>
        </div>
    )
    }
    const lastPage = !steps[this.state.currentPageNumber + 1]
      return (
        <div style={{maxWidth: '900px'}}>
            {this.state.currentPageNumber === 0 ? this.introMessage() : ''}
            <StepsPage
              lastPage={lastPage}
              page={this.getPageInfos()}
              pageNumber={this.state.currentPageNumber}
              previousPage={this.previousPage}
              onSubmit={!lastPage ? this.nextPage : this.onSubmit}
            />
        </div>
      )
  }
}

function mapStateToProps(state) {
  return { form: state.form, server: state.server }
}

export default reduxForm({
  form: 'dossier',
  destroyOnUnmount: false
})(connect(mapStateToProps, actions)(Steps))
