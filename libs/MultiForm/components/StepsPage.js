// Show a single page
import React, { Component } from 'react'
import { reduxForm, Field, SubmissionError } from 'redux-form'
import { connect } from 'react-redux'
import genericErrors from '../data/genericErrors'

import _ from 'lodash'

import StepsField from './StepsField'

let FIELDS = []
const REGEX = {
  email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  url: /(http(s)?)/gi,
  numeric: /[0-9]*/,
  alpha: /[a-zA-Z]*/,
  codePostal: /[0-9]{5}/
}

class StepsPage extends Component
{
  constructor(props) {
    super(props)
    this.state = {locked: true, errors: []}
    this.printButtons = this.printButtons.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
    this.renderErrorBloc = this.renderErrorBloc.bind(this)
  }

  onSubmit(e) {
    e.preventDefault()
    this.setState({errors: []})
    return new Promise((resolve, reject) => {
      const dossier = this.props.form.dossier
      let errors = {}
      if (dossier.anyTouched) {
        errors = this.props.validate(dossier.values)
        this.setState({ errors })
        if (!_.isEmpty(errors)) {
          return reject(errors)
        } else {
        return resolve(this.props.handleSubmit(e))
        }
      }
    })
  }

  printErrorBloc (lastPage) {
    return lastPage && this.state.errors.length && this.state.errors.map((err, i) => {
      <p key={i}>err</p>
    })
  }

  printButtons(pageNumber, lastPage) {
    const { dossier } = this.props.form
    const hasError = !!this.state.errors.length
    const backButton = pageNumber ? <button disabled={hasError} className="left red white-text btn flat-btn" onClick={this.props.previousPage}>Retour</button> : ''
    const submitButton = lastPage ? <button disabled={hasError} type="submit" className="right white-text btn flat-btn">Soumettre</button> : <button type="submit" disabled={hasError || this.state.locked} className="right white-text btn flat-btn" >Suivant</button>
    return (
      <div style={{marginTop: '20px'}}>
         {submitButton}
         {backButton}
      </div>
    )
  }

  renderFields() {
    const { page } = this.props
    const fields = _.map(page.fields, (field, index) => {
      return (
        <Field
          _type={field.type}
          key={index}
          error={this.state.errors[field._name]}
          items={field.items || []}
          dynLabel={field.dyn_label}
          name={field._name || field.name}
          label={field.label}
          rules={field.rules}
          title={field.title || field.name || 'untitled'}
          sources={field.sources || []}
          component={StepsField}
          onChange={() => {
            let errors = {...this.state.errors}
            errors[field._name] = null
            this.setState({locked: false, errors})
          }}
        />
      )
    })
    return (
      <div>
       {fields}
      </div>
    )
  }

  renderPage(page) {
    if (!page) return <p>End of form</p>
    return (
      <div>
        { this.renderFields() }
      </div>
    )
  }

  renderErrorBloc (lastPage) {
    let errors = false
    _.each(this.state.errors, (e) => {
      if (e && e.length) {
        errors = true
        return false
      }
    })
    if (lastPage && errors) {
      return (
        <div className="errorBloc alert danger">
          <p>Il y a des erreurs dans le formulaire</p>
        </div>
      )
    }
  }

  renderCalendar (cat) {
    let title = 'Culture et Sport'
    if (cat === 'Culture') {
      title = 'Culture'
    } else if (cat === 'Sport') {
      title = 'Sport'
    }
    return (
       <table style={{ marginBottom: '15px' }} className="striped">
        <thead>
          <tr>
              <th>Date limite de dépôt de dossier</th>
              <th>Réunion Comité {title} </th>
              <th>Réponse aux porteurs de projet</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>15 mars</td>
            <td>1ère quinzaine d'avril</td>
            <td>30 avril</td>
          </tr>
          <tr>
            <td>15 octobre</td>
            <td>1ère quinzaine de novembre</td>
            <td>30 novembre</td>
          </tr>
        </tbody>
      </table>
    )
  }

  calendar (pageNumber = null, categorie = null) {
    if (pageNumber === 1 && categorie === 'Culture') {
      return this.renderCalendar('Culture')
    } else if( pageNumber === 1 && categorie === 'Sport') {
      return this.renderCalendar('Sport')
    } else {
      return null
    }
  }

  render() {
    const { page, pageNumber, form, lastPage } = this.props
    console.log(page, form)
    FIELDS = this.props.page.fields
    return (
      <div className={`page_${pageNumber}`}>
      {this.calendar(pageNumber, form.dossier && form.dossier.values && form.dossier.values['0_categorie'])}
      <form onSubmit={this.onSubmit}>
        {this.renderPage(page)}
        {this.renderErrorBloc(lastPage)}
        {this.printButtons(pageNumber, lastPage)}
      </form>
      </div>
      )
  }
}

function validate(values) {
  const err = {}
  _.each(FIELDS, ({ _name, rules = {}, errors = {}}) => {
    const {required, mustBe, dyn, size, type, min, max} = rules
    const value = values[_name] ? (typeof values[_name] === String ? values[_name].trim() : values[_name]) : false
    // Required validation
    if (required && !value) err[_name] = errors.required || genericErrors('required')
    // Size validation
    if (size && value.length !== size) err[_name] = errors.size || genericErrors('size')
    // Type validation
    if (type && !REGEX[type].test(value)) err[_name] = errors[type] || genericErrors(type)
    // min validation
    if (min && value.length < min) err[_name] = errors.min || genericErrors('min')
    // max validation
    if (max && value.length > max) err[_name] = errors.max || genericErrors('max')
    // mustBe validation
    if (mustBe && value !== mustBe) err[_name] = errors.mustBe || genericErrors('mustBe')
     // dyn validation
     if (dyn && value && value.split(dyn.sep).length != dyn.count) err[_name] = errors.dyn || genericErrors('dyn')
  })
  return err
}

function mapStateToProps({ form }) {
  return { form: form }
}

export default reduxForm({
  validate,
  form: 'dossier',
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true
})(connect(mapStateToProps)(StepsPage))
