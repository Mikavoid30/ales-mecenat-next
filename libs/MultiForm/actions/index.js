import axios from 'axios'
import _ from 'lodash'

import { SUBMIT_FORM } from './types'

export const submitForm = (values, steps) => async dispatch => {

      const sections = [{ replies: [] }]
      let i = 0;

      // Récupérer toutes les réponses
      const replies = []
      _.each(steps, (step) => {
        // Je récupère les réponses liées à cette étapes
        _.each(step.fields, (field) => {
          if (field.type) replies.push({type: field.type || 'title', name: field._name, label: field.title || field.label || 'untitled', value: values[field._name]})
        })
      })
      const length = replies.length
      let currentSection = sections[0]
      for (i = 0; i < length; i++) {
        if (replies[i].type === 'title') {
          currentSection = { title: replies[i].label, replies: [] }
          sections.push(currentSection)
        } else {
          currentSection.replies.push(replies[i])
        }
      }
      const res = await axios.post('http://mickael-boulat.fr:5000/api/dossiers', sections)
      // const res = await axios.post('http://localhost:5000/api/dossiers', sections)
      dispatch({type: SUBMIT_FORM, payload: {status: res.status}})
    }
