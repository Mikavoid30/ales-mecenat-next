import genericErrors from './genericErrors'

const free_comment = [
  {
    type: 'title',
    label: "Commentaire libre"
  },
  {
    type: 'textarea',
    label: 'Avez-vous quelque chose à rajouter ?',
    name: 'freecomment',
    rules: {required: false},
  },
]
const optionnal_files = [
  {
    type: 'title',
    label: "Fichiers optionnels / annexes"
  },
  {
    type: 'file',
    title: 'Fichier annexe 1',
    label: 'Annexe 1',
    name: 'annexe_1',
    rules: {required: false, maxSize: 2, extensions: 'pdf, doc, docx, xls, xlsx'.split(', ')}
  },
  {
    type: 'file',
    title: 'Fichier annexe 2',
    label: 'Annexe 2',
    name: 'annexe_2',
    rules: {required: false, maxSize: 2, extensions: 'pdf, doc, docx, xls, xlsx'.split(', ')}
  },
  {
    type: 'file',
    title: 'Fichier annexe 3',
    label: 'Annexe 3',
    name: 'annexe_3',
    rules: {required: false, maxSize: 2, extensions: 'pdf, doc, docx, xls, xlsx'.split(', ')}
  },
  {
    type: 'file',
    title: 'Fichier annexe 4',
    label: 'Annexe 4',
    name: 'annexe_4',
    rules: {required: false, maxSize: 2, extensions: 'pdf, doc, docx, xls, xlsx'.split(', ')}
  },
  {
    type: 'file',
    title: 'Fichier annexe 5',
    label: 'Annexe 5',
    name: 'annexe_5',
    rules: {required: false, maxSize: 2, extensions: 'pdf, doc, docx, xls, xlsx'.split(', ')}
  }
]
const generic_infos = [
  {
    type: 'title',
    label: 'Informations générales'
  },
  {
    type: 'text',
    title: 'Statut Juridique',
    label: 'Statut Juridique',
    name: 'statutJuridique',
    rules: {required: true}
  },
  {
    type: 'text',
    title: 'SIRET',
    label: 'Numéro SIRET',
    name: 'siret',
    rules: {required: true},
    errors: {
      required: 'Merci de bien vouloir saisir votre numéro SIRET',
      size: 'Ce numéro de SIRET n\'est pas au bon format'
    }
  },
  {
    type: 'text',
    title: 'Représentant légal',
    label: 'Nom du représentant légal',
    name: 'representantLegal',
    rules: {required: true}
  },
  {
    type: 'textarea',
    label: 'Adresse du siège social',
    name: 'siegeSocial',
    rules: {required: true}
  },
  {
    type: 'text',
    label: 'Code Postal',
    name: 'codePostalSiege',
    rules: {required: true, size: 5},
    errors: {
      required: 'Merci de bien vouloir saisir le code postal de votre siège social',
      size: 'Ce code postal n\'est pas au bon format'
    }
  },
  {
    type: 'text',
    label: 'Téléphone',
    name: 'telephone',
    rules: {required: true, min: 10, type: 'numeric'},
    errors: {
      min: 'Un numéro de téléphone est composé d\'au moins 10 caractères'
    }
  },
  {
    type: 'text',
    label: 'Ville',
    name: 'ville',
    rules: {required: true, min: 2},
    errors: {
      required: 'Merci de bien vouloir saisir la ville de votre siège social',
      min: genericErrors('min')
    }
  },

  {
    type: 'text',
    label: 'Courriel',
    name: 'courriel',
    rules: {required: true, min: 5, type: 'email'},
    errors: {
      type: 'Votre adresse électronique n\'est pas au bon format',
      required: 'Merci de bien vouloir saisir votre adresse email',
      min: genericErrors('min')
    }
  },

  {
    type: 'text',
    label: 'Site Internet',
    name: 'siteinternet',
    rules: {required: false},
    errors: {
      type: 'Ceci n\'est pas une URL valide'
    }
  },

  {
    type: 'text',
    label: 'Personne contact (nom, tél, mail)',
    name: 'personnecontact',
    rules: {required: true, min: 2},
  },

  {
    type: 'file',
    title: 'Rescrit Fiscal',
    label: 'Votre structure	est reconnue d\'intêret général,	joindre	le rescrit fiscal',
    name: 'rescritFiscal',
    rules: {required: true, maxSize: 2, extensions: 'pdf, xls, xlsx, doc, docx'.split(', ')}
  },
  {
    type: 'file',
    title: 'Statuts',
    label: 'Statuts',
    name: 'statuts',
    rules: {required: true, maxSize: 2, extensions: 'pdf, xls, xlsx, doc, docx'.split(', ')}
  }
]

const generic_elements_budgetaires = [
  {
    type: 'title',
    label: 'Éléments budgétaires'
  },
  {
    type: 'textarea',
    label: 'Est-il prévu une participation financière des publics? Si oui, laquelle? ',
    name: 'participation',
    rules: {required: true},
  },
  {
    type: 'textarea',
    label: 'Montant de la dotation sollicitée auprès d\'Alès Mécénat ',
    name: 'montant',
    rules: {required: true},
  },
  {
    type: 'textarea',
    label: 'D\'autres financements ont-ils été sollicités (privé(s) public(s) Si oui, lesquels ?',
    name: 'autresFinancements',
    rules: {required: true},
  },
  {
    type: 'file',
    title: 'Budget prévisionnel',
    label: 'Joindre un budget prévisionnel selon le modèle fourni',
    name: 'budgetPrevisionnel',
    rules: {required: true, maxSize: 2, extensions: 'xls, xlsx'.split(', ')}
  }
]

export const page_0 = [
  {
    type: 'select',
    label: 'Catégorie de votre projet',
    title: 'Catégorie du projet',
    name: 'categorie',
    items: [
      {value: '', label: 'Selectionner votre catégorie.'},
      {value: 'Culture'},
      {value: 'Sport'}
    ],
    rules: {required: true},
    errors: {
      required: 'Merci de bien vouloir sélectionner une catégorie.'
    }
  }
]

export const page_1 = [
  {
    type: 'text',
    label: 'Quel est le nom de la structure ?',
    title: 'Nom de la structure',
    name: 'nomStructure',
    rules: {required: true},
    errors: {
      required: 'Merci de bien vouloir renseigner le nom de votre structure.'
    }
  },
  {
    type: 'radio',
    title: 'Structure d\'intêret général ?',
    label: 'Vous structure est-t-elle reconnue d\'intêret général ?',
    name: 'interetGeneral',
    items: [
      {value: 'Oui'},
      {value: 'Non'}
    ],
    rules: {required: true, mustBe: 'Oui'},
    errors: {
      required: 'Merci de bien vouloir sélectionner une réponse',
      mustBe: 'Vous ne pouvez pas poursuivre votre demande si votre structure n\'est pas reconnue d\'intêret général'
    }
  }
]

export const page_2 = [
  {
    type: 'text_dynamic',
    title: 'Code Postal - Ville',
    label: 'Code postal de votre commune',
    name: 'codePostal',
    dyn_label: 'Votre ville',
    rules: {
      required: true,
      type: 'codePostal',
      dyn : {
        sep: ' - ',
        count: 2
      }
    },
    errors: {
      required: 'Merci de bien vouloir sélectionner votre code postal',
      dyn: 'Merci de saisir une code postal et sélectionner une ville éligible'
    },
    sources: {
      data: 'cities',
      output: 'select' // En fonction du code postal, on va rechercher dans le fichier cities, la ville correspondant et on ajoute un select avec ces villes
    }
  }
]

export const page_3_culture = [
  ...generic_infos,
  {
    type: 'title',
    label: "Présentation du projet"
  },
  {
    type: 'textarea',
    label: 'Intitulé du projet',
    name: 'intitule',
    rules: {required: true},
  },
  {
    type: 'checkboxes',
    label: 'Domaines concernés',
    name: 'domaines',
    items : [
      {value: 'Spectacle Vivant'},
      {value: 'Arts visuels'},
      {value: 'Cinéma'},
      {value: 'Musique'},
      {value: 'Culture'},
      {value: 'Scientifique'}
    ],
    rules: {required: true},
  },
  {
    type: 'textarea',
    label: 'Autres domaines',
    name: 'autresDomaines',
    rules: {required: false},
  },
  {
    type: 'textarea',
    label: 'Descriptif du projet',
    name: 'descriptif',
    rules: {required: true},
  },
  {
    type: 'textarea',
    label: 'Date de mise en oeuvre',
    name: 'date',
    rules: {required: true},
  },
  {
    type: 'textarea',
    label: 'Durée de l\'action',
    name: 'dureeaction',
    rules: {required: true},
  },
  {
    type: 'textarea',
    label: 'Objectifs',
    name: 'objectifs',
    rules: {required: true},
  },
  {
    type: 'textarea',
    label: 'En quoi le projet contribue-t-il à l\'image et au rayonnement du territoire ?',
    name: 'rayonnement',
    rules: {required: true},
  },
  {
    type: 'textarea',
    label: 'Publics concernés, nombre de personnes attendues',
    name: 'publics',
    rules: {required: true},
  },
  {
    type: 'textarea',
    label: 'Adéquation du projet avec le public ciblé',
    name: 'adequation',
    rules: {required: true},
  },
  {
    type: 'textarea',
    label: 'Quel est le caractère innovant du projet ?',
    name: 'innovant',
    rules: {required: false},
  },
  {
    type: 'textarea',
    label: 'Apports du projet à l\'offre culturelle existante',
    name: 'apports',
    rules: {required: true},
  },
  {
    type: 'textarea',
    label: 'Composition et description de l\'équipe affectée au projet',
    name: 'composition',
    rules: {required: true},
  },
  ...generic_elements_budgetaires,
  ...free_comment,
  ...optionnal_files

]

export const page_3_sport= [
  ...generic_infos,
  {
    type: 'title',
    label: "Présentation du projet"
  },
  {
    type: 'textarea',
    label: 'Intitulé du projet',
    name: 'intitule',
    rules: {required: true},
  },
  {
    type: 'textarea',
    label: 'Portée du projet',
    name: 'portee',
    rules: {required: true},
  },
  {
    type: 'textarea',
    label: 'Porteur du projet',
    name: 'porteur',
    rules: {required: true},
  },
  {
    type: 'textarea',
    label: 'Discipline(s) sportive(s) concernée(s) (tennis, natation athlétisme...)',
    name: 'disciplines',
    rules: {required: true},
  },
  {
    type: 'checkboxes',
    label: 'Domaines concernés',
    name: 'domaines',
    items : [
      {value: 'Education / Citoyenneté'},
      {value: 'Social'},
      {value: 'Santé / Handicap'},
      {value: 'Economique'},
      {value: 'Femme et sport'}
    ],
    rules: {required: true},
  },

  {
    type: 'title',
    label: 'Le projet et le territoire'
  },
  {
    type: 'radio',
    label: 'Contribue à l\'image du territoire (notoriété)',
    name: 'image',
    items : [
      {value: 'Oui'},
      {value: 'Non'}
    ],
    rules: {required: true}
  },
  {
    type: 'radio',
    label: 'Contribue à la dynamique du territoire (vie et attractivité)',
    name: 'dynamique',
    items : [
      {value: 'Oui'},
      {value: 'Non'}
    ],
    rules: {required: true}
  },
  {
    type: 'radio',
    label: 'Contribue au développement de la pratique sportive et de loisirs du territoire',
    name: 'developpement',
    items : [
      {value: 'Oui'},
      {value: 'Non'}
    ],
    rules: {required: true}
  },
  {
    type: 'radio',
    label: 'S\'inscrit dans le cadre du développement durable',
    name: 'durable',
    items : [
      {value: 'Oui'},
      {value: 'Non'}
    ],
    rules: {required: true}
  },
  {
    type: 'title',
    label: 'Spécificités du projet'
  },
  {
    type: 'textarea',
    label: 'Projet innovant',
    name: 'projetInnovant',
    rules: {required: false},
  },
  {
    type: 'select',
    label: 'Durée du projet ou de l\'action ',
    name: 'dureeAction',
    items: [
      {value: 'Ponctuelle 1 jour ou +'},
      {value: 'Durable (reproduit d\'une année sur l\'autre)'},
      {value: 'Autre'}
    ],
    rules: {required: true},
  },
  {
    type: 'textarea',
    label: 'Développer les modalités',
    name: 'modalites',
    rules: {required: false},
  },
  {
    type: 'textarea',
    label: 'Contraintes/Moyens - Resources Humaines',
    name: 'rh',
    rules: {required: true},
  },
  {
    type: 'textarea',
    label: 'Contraintes/Moyens - Matériels',
    name: 'materiel',
    rules: {required: true},
  },
  {
    type: 'textarea',
    label: 'Contraintes/Moyens - Logistique',
    name: 'logistique',
    rules: {required: true},
  },
  {
    type: 'textarea',
    label: 'Coût estimé du projet et faisabilité',
    name: 'faisabilité',
    rules: {required: false},
  },
  {
    type: 'checkboxes',
    label: 'Pratiquants concernés',
    name: 'pratiquuants',
    items: [
      {value: 'Tous pratiquants'},
      {value: 'Haut niveau'},
      {value: 'Jeunes'},
      {value: 'Femmes'},
      {value: 'Séniors'}
    ],
    rules: {required: false},
  },
  {
    type: 'textarea',
    label: 'Public estimé (quantitatif)',
    name: 'publicquantitatif',
    rules: {required: true},
  },
  {
    type: 'textarea',
    label: 'Type de public',
    name: 'publictype',
    rules: {required: true},
  },
  {
    type: 'textarea',
    label: 'Impact du projet sur la sphère commerciale',
    name: 'impacts',
    rules: {required: true},
  },
  ...generic_elements_budgetaires,
  ...free_comment,
  ...optionnal_files
]
