export default (field) => {
  const errors = {
    min: 'Pas assez de caractères',
    max: 'Trop de caractères',
    required: 'Ce champ est obligatoire',
    size: 'Ce champ ne possède pas le bon nombre de caractères',
    type: 'Ce champ n\'est pas au bon format',
    dyn: 'Ce choix est impossible',
    mustBe : 'Ce choix est impossible'
  }

  if (errors[field]) return errors[field]
  return 'Error'
}