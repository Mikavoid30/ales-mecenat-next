
const GENERIC_ERRS = {
  min: 'Pas assez de caractères',
  max: 'Trop de caractères',
  required: 'Ce champ est obligatoire',
  size: 'Ce champ ne possède pas le bon nombre de caractères'
}

export const page_0 = [
  {
    type: 'select',
    label: 'Catégorie de votre projet',
    title: 'Catégorie du projet',
    name: 'categorie',
    items: [
      {value: '', label: 'Selectionner votre catégorie.'},
      {value: 'Culture'},
      {value: 'Sport'}
    ],
    rules: {required: true},
    errors: {
      required: 'Merci de bien vouloir sélectionner une catégorie.'
    }
  }
]

export const page_1 = [
  {
    type: 'text',
    label: 'Quel est le nom de la structure ?',
    title: 'Nom de la structure',
    name: 'nomStructure',
    rules: {required: true},
    errors: {
      required: 'Merci de bien vouloir renseigner le nom de votre structure.'
    }
  },
  {
    type: 'radio',
    title: 'Structure d\'intêret général ?',
    label: 'Vous structure est-t-elle reconnue d\'intêret général ?',
    name: 'interetGeneral',
    items: [
      {value: 'Oui'},
      {value: 'Non'}
    ],
    rules: {required: true, mustBe: 'Oui'},
    errors: {
      required: 'Merci de bien vouloir sélectionner une réponse',
      mustBe: 'Vous ne pouvez pas poursuivre votre demande si votre structure n\'est pas reconnue d\'interet général'
    }
  }
]

export const page_2 = [
  {
    type: 'text_dynamic',
    title: 'Code Postal - Ville',
    label: 'Code postal de votre commune*',
    name: 'codePostal',
    dyn_label: 'Votre ville',
    rules: {
      required: true,
      dyn : {
        sep: ' - ',
        count: 2
      }
    },
    errors: {
      required: 'Merci de bien vouloir sélectionner votre code postal',
      dyn: 'Merci de saisir une code postal et sélectionner une ville éligible'
    },
    sources: {
      data: 'cities',
      output: 'select' // En fonction du code postal, on va rechercher dans le fichier cities, la ville correspondant et on ajoute un select avec ces villes
    }
  }
]

export const page_3_culture = [
  {
    type: 'text',
    title: 'Statut Juridique',
    label: 'Statut Juridique',
    name: 'statutJuridique',
    rules: {required: true}
  },
  {
    type: 'text',
    title: 'SIRET',
    label: 'Numéro SIRET',
    name: 'siret',
    rules: {required: true, size: 14},
    errors: {
      required: 'Merci de bien vouloir saisir votre numéro SIRET',
      size: 'Ce numéro de SIRET n\'est pas au bon format'
    }
  },
  {
    type: 'text',
    title: 'Représentant légal',
    label: 'Nom du représentant légal',
    name: 'representantLegal',
    rules: {required: true}
  },
  {
    type: 'textarea',
    label: 'Adresse du siège social',
    name: 'siegeSocial',
    rules: {required: true}
  },
  {
    type: 'text',
    label: 'Code Postal',
    name: 'codePostalSiege',
    rules: {required: true, size: 5},
    errors: {
      required: 'Merci de bien vouloir saisir le code postal de votre siège social',
      size: 'Ce code postal n\'est pas au bon format'
    }
  },
  {
    type: 'text',
    label: 'Téléphone',
    name: 'telephone',
    rules: {required: true, min: 10},
    errors: {
      required: 'Merci de bien vouloir saisir le code postal de votre siège social',
      min: 'Un numéro de téléphone est composé d\'au moins 10 caractères'
    }
  },
  {
    type: 'text',
    label: 'Ville',
    name: 'ville',
    rules: {required: true, min: 2},
    errors: {
      required: 'Merci de bien vouloir saisir la ville de votre siège social',
      min: GENERIC_ERRS.min
    }
  },

  {
    type: 'text',
    label: 'Courriel',
    name: 'courriel',
    rules: {required: true, min: 5, type: 'email'},
    errors: {
      type: 'Votre adresse electronique n\'est pas au bon format',
      required: 'Merci de bien vouloir saisir votre adresse email',
      min: GENERIC_ERRS.min
    }
  },

  {
    type: 'text',
    label: 'Site Internet',
    name: 'siteinternet',
    rules: {required: false, type: 'url'},
    errors: {
      type: 'Ceci n\'est pas une URL valide'
    }
  },

  {
    type: 'text',
    label: 'Personne contact (nom, tél, mail)',
    name: 'personnecontact',
    rules: {required: true, min: 2},
  },
  
  {
    type: 'file',
    title: 'Rescrit Fiscal',
    label: 'Votre structure	est reconnue d\'interet general,	joindre	le rescrit fiscal',
    name: 'rescritFiscal',
    rules: {required: true, extensions: 'pdf, xls, xlsx, doc, docx'.split(', ')}
  },
  {
    type: 'file',
    title: 'Statuts',
    label: 'Statuts',
    name: 'statuts',
    rules: {required: true, extensions: 'pdf, xls, xlsx, doc, docx'.split(', ')}
  }
]

export const page_3_sport= [
  {
    type: 'text',
    label: 'Sport',
    title: 'Spooort',
    name: 'sport',
    rules: {
      required: true,
      extensions: ['txt', 'pdf', 'doc', 'docx', 'xls', 'xlsx', 'ods', 'odt']
    }
  },
  {
    type: 'file',
    title: 'Rescrit Fiscal',
    label: 'Rescrit',
    name: 'rescritFiscal',
    rules: {
      required: true,
      extensions: ['txt', 'pdf', 'doc', 'docx', 'xls', 'xlsx', 'ods', 'odt']
    }
  }
]
