import 'isomorphic-fetch'

function _ (path: string) {
  const url = process.env.API_URL || 'http://localhost:8000/wp-json/wp/v2'
  return `${url}/${path}`
}

export async function getPosts (nbPosts = 10, page = 1) {
  const response = await fetch(_('posts?_embed'))
  console.log('response', response)
  const data: any = await response.json()

  return data.filter((post: WP_POST) => {
    return post.status === 'publish' && post.type === 'post'
  })
}

export default {
  getPosts
}
