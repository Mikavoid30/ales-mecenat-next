import React from 'react'

import _ from './TextSection.scss'
import Section from '../../UI/Section/Section'
import Headline from '../../UI/Headline/Headline'
import Text from '../../UI/Text/Text'
import Button from '../../UI/Button/Button'

type TextSectionProps = {
  title?: string
  text?: string
  button?: { label: string; href: string }
  children?: JSX.Element | JSX.Element[]
}

function TextSection ({ title, text, button }: TextSectionProps): JSX.Element {
  return (
    <div className={_.TextSection}>
      <Section
        light={true}
        spaced
        style={{ paddingTop: '160px' }}
        wave='/static/images/arrow-bottom.svg'
      >
        <>
          {title && (
            <Headline tag={'h1'} primary>
              {title}
            </Headline>
          )}
          <div className={_.LittleWord}>
            {text && <Text>{text}</Text>}
            {button && <Button primary>{button.label}</Button>}
          </div>
        </>
      </Section>
    </div>
  )
}

export default TextSection
