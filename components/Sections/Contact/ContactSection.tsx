import React from 'react'

import _ from './ContactSection.scss'
import Headline from '../../UI/Headline/Headline'
import Text from '../../UI/Text/Text'
import Section from '../../UI/Section/Section'
import ContactForm from '../../ContactForm/ContactForm'
import Button from '../../UI/Button/Button'
import Zoom from 'react-reveal/Zoom'

type ContactSectionProps = {
  spaced?: boolean
}

function ContactSection ({ spaced }: ContactSectionProps): JSX.Element {
  return (
    <div style={{ position: 'relative' }}>
      <Section light wave='/static/images/arrow-bottom.svg'>
        <div className={_.ContactSection}>
          {' '}
          <div className={_.Infos}>
            <Headline tag='h3' noPadding>
              Alès Mécénat
            </Headline>
            <Text>
              Alès Mécénat, un fonds de dotation unique en France dédié à la
              culture et au sport. Piloté par des membres de Leader Alès et
              co-animé par les principaux partenaires de la culture et du sport,
              son objectif affiché est de réunir et de structurer les démarches
              de mécénat sur le territoire alésien avec une validation par un
              comité technique piloté par des spécialistes puis une validation
              financière.
            </Text>
            <Text>
              Pour les entreprises c’est l’assurance d’avoir des projets
              structurés ou en voie de structuration, une lisibilité des actions
              proposées, et la capacité de flécher les dotations sur les projets
              qu’elles souhaitent accompagner collectivement ou
              individuellement. C’est également la possibilité de contribuer
              pour 20% des sommes à un fonds commun pour accompagner des projets
              innovants.
            </Text>
            <Button size='S' primary>
              Voir les membres du comité d’administration
            </Button>
          </div>
          <div className={_.Logo}>
            <Zoom>
              <img src='/static/images/logo_amecenat.svg' />
            </Zoom>
          </div>
          <div className={_.Form}>
            <Headline tag='h3' noPadding>
              Contact
            </Headline>
            <ContactForm />
          </div>
        </div>
      </Section>
    </div>
  )
}

export default ContactSection
