import React from 'react'

import _ from './PlanningSection.scss'
import Section from '../../UI/Section/Section'
import Headline from '../../UI/Headline/Headline'

import Zoom from 'react-reveal/Zoom'
import MiniPost from '../../MiniPost/MiniPost'

type PlanningSectionProps = {}

const data = {
  limitDates: ['03-15', '10-15'],
  answerDates: ['04-30', '11-30'],
  meetingDates: ['04-15', '11-15']
}

function getNextDate (meetingDate: string = data.limitDates[0]): Date {
  let currentYear = new Date().getFullYear()
  const dateTs = new Date(`${currentYear}-${meetingDate}`).getTime()
  const now = Date.now()

  if (dateTs <= now) currentYear += 1

  return new Date(`${currentYear}-${meetingDate}`)
}

function formatDate (date: Date): string {
  return date.toLocaleDateString('fr-FR', {
    year: 'numeric',
    month: 'long',
    day: 'numeric'
  })
}

function generateDatesElements (
  dates: string[],
  prefix?: string,
  nextPrefix: string = 'La prochaine le'
) {
  const timestamps = dates.map((d: string) => getNextDate(d).getTime())
  const sorted = timestamps.sort((a, b) => a - b)
  const lgt = timestamps.length
  return sorted.map((ts, idx) => {
    let pre = prefix || ''
    if (idx === lgt - 1) pre = nextPrefix
    return (
      <p>
        {pre} {formatDate(new Date(ts))}
      </p>
    )
  })
}

function PlanningSection (props: PlanningSectionProps): JSX.Element {
  const limitDates = generateDatesElements(data.limitDates)
  const answerDates = generateDatesElements(data.answerDates)
  const meetingDates = generateDatesElements(
    data.meetingDates,
    '01 - ',
    'Puis 01 - '
  )
  return (
    <div className={_.PlanningSection}>
      <Section
        light={true}
        spaced
        style={{ paddingBottom: '80px' }}
        wave='/static/images/arrow-bottom.svg'
      >
        <>
          <Headline tag={'h1'} primary>
            Planning
          </Headline>

          <div className={_.Grid}>
            <div className={_.Column3}>
              <Zoom>
                <h4>RÉUNIONS COMITÉ CULTURE ET SPORT</h4>
                <div className={_.Date}>{meetingDates}</div>
              </Zoom>
            </div>
            <div className={_.Column3}>
              {' '}
              <Zoom>
                <h4>DATES LIMITE DE DÉPÔT DE DOSSIER</h4>
                <div className={_.Date}>{limitDates}</div>
              </Zoom>
            </div>
            <div className={_.Column3}>
              {' '}
              <Zoom>
                <h4>RÉPONSES AUX PORTEURS DE PROJET</h4>
                <div className={_.Date}>{answerDates}</div>
              </Zoom>
            </div>
          </div>
        </>
      </Section>
    </div>
  )
}

export default PlanningSection
