import React from 'react'
import Headline from '../../UI/Headline/Headline'
import { useRouter } from 'next/router'

import style from './Services.scss'
import Service from '../../Service/Service'
import Button from '../../UI/Button/Button'
import FlexGroup from '../../UI/FlexGroup/FlexGroup'
import Section from '../../UI/Section/Section'
import Router from 'next/dist/next-server/server/router'

const data = {
  culture: {
    title: 'Défendre les projets culturels locaux',
    text:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mollis ante eget lorem sodales, vitae rhoncus nisi vehicula. Quisque ac dolor ullamcorper, volutpat ex non, luctus est. Donec dignissim orci eu felis commodo varius at a velit. Praesent sed erat dui. Nullam facilisis eu nibh vel tincidunt. Donec eget malesuada tortor. Phasellus aliquam tellus nec turpis scelerisque, vel placerat dolor semper'
  },
  sport: {
    title: 'Encourager des projets sportifs',
    text:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mollis ante eget lorem sodales, vitae rhoncus nisi vehicula. Quisque ac dolor ullamcorper, volutpat ex non, luctus est. Donec dignissim orci eu felis commodo varius at a velit. Praesent sed erat dui. Nullam facilisis eu nibh vel tincidunt. Donec eget malesuada tortor. Phasellus aliquam tellus nec turpis scelerisque, vel placerat dolor semper'
  }
}

function ServicesSection () {
  const router = useRouter()
  return (
    <section className={style.ServicesSection}>
      <Section light spaced wave='/static/images/arrow-bottom.svg'>
        <>
          <Headline tag='h1' primary>
            Nos Ambitions
          </Headline>
          <div className={style.Container}>
            {/* Culture */}
            <Service
              imagePosition='left'
              image='/static/images/culture_orange.svg'
              data={data.culture}
            >
              <FlexGroup align='left'>
                <Button primary onClick={() => router.push('/culture')}>
                  Comité culture
                </Button>
                <Button accent>Déposer un dossier</Button>
              </FlexGroup>
            </Service>

            {/* Sport */}
            <Service
              imagePosition='right'
              image='/static/images/sports_orange.svg'
              data={data.sport}
            >
              <FlexGroup align='right'>
                <Button primary onClick={() => router.push('/sport')}>
                  {' '}
                  Comité sport
                </Button>
                <Button accent>Déposer un dossier</Button>
              </FlexGroup>
            </Service>
          </div>
        </>
      </Section>
    </section>
  )
}

export default ServicesSection
