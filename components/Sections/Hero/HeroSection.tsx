import React from 'react'
import classes from './HeroSection.scss'
import Fade from 'react-reveal/Fade'
import Swing from 'react-reveal/Swing'

function HeroSection () {
  const data = {
    site: {
      siteMetadata: {
        subtitle: '',
        video: 'video'
      }
    }
  }
  return (
    <section className={classes.Hero}>
      <div className={classes.HeroBg}></div>
      <div className={classes.Container}>
        <div className={classes.Branding}>
          <div className={classes.Title}>
            <Fade>
              Alès{' '}
              <Fade delay={300}>
                <span>Mécénat</span>
              </Fade>
            </Fade>
          </div>
          <div className={classes.SubTitle}>
            {data.site.siteMetadata.subtitle}
          </div>
        </div>
        <div className={classes.Video}>
          <div className={classes.VideoContainer}>
            <span className={classes.VideoTag}>
              <Swing delay={1500}>Découvrez nous en vidéo</Swing>
            </span>
            <iframe
              width='100%'
              height='100%'
              src='https://www.youtube.com/embed/sjWXZ--8PRY'
              allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture'
            ></iframe>
          </div>
        </div>
      </div>
    </section>
  )
}

export default HeroSection
