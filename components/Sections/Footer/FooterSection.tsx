import React from 'react'

import _ from './FooterSection.scss'
import Button from '../../UI/Button/Button'
import Link from 'next/link'
import Section from '../../UI/Section/Section'

type FooterSectionProps = {}

function FooterSection (props: FooterSectionProps): JSX.Element {
  return (
    <Section spaced>
      <footer className={_.FooterSection}>
        <div className={_.Button}>
          <Button accent>Déposer un dossier</Button>
        </div>
        <div className={_.Infos}>
          <div className={_.Author}>
            <p className={_.Copyrights}>Tous droits réservés - Alès Mécénat {new Date().getFullYear()}</p>
            <em>
              Site réalisé par{' '}
              <Link href='https://www.mickael-boulat.fr'>
                <a target='_blank'>Mickaël BOULAT</a>
              </Link>
            </em>
          </div>
          <div className={_.Socials}>
            <ul>
              <li>
                <Link href='https://www.facebook.com/AlesMecenat'><a target="_blank">Facebook</a></Link>
              </li>
              <li>
                <Link href='https://twitter.com/AlesMecenat'><a target="_blank">Twitter</a></Link>
              </li>
              <li>
                <Link href='https://www.linkedin.com/company/al%C3%A8s-m%C3%A9c%C3%A9nat/'><a target="_blank">LinkedIn</a></Link>
              </li>
              <li>
                <Link href='https://www.youtube.com/channel/UChybzzakQlKZ5de8e7fgKLQ'><a target="_blank">Youtube</a></Link>
              </li>
            </ul>
          </div>
        </div>
      </footer>
    </Section>
  )
}

export default FooterSection
