import React from 'react'

import _ from './Mecenes.scss'
import Headline from '../../UI/Headline/Headline'
import ImageGrid from '../../ImageGrid/ImageGrid'
import Section from '../../UI/Section/Section'
import Text from '../../UI/Text/Text'
import Fade from 'react-reveal/Fade'

const IMG_FOLDER = '/static/images/mecenes'

type MecenesProps = {
  spaced?: boolean
}

const data = [
  {
    name: 'MSL',
    url: 'http://www.msl-stores.com/',
    src: `${IMG_FOLDER}/msl.png`
  },
  {
    name: 'Hurricane',
    url: 'http://www.hurricane.fr/',
    src: `${IMG_FOLDER}/hurricane.png`
  },
  {
    name: 'Veolia',
    url: 'https://www.service.eau.veolia.fr/home.html',
    src: `${IMG_FOLDER}/veolia.png`
  },
  {
    name: 'NTN SNR',
    url: 'https://www.ntn-snr.com/fr',
    src: `${IMG_FOLDER}/snr.png`
  },
  {
    name: 'Ozil Conseil',
    url: 'http://ozil-conseil.com',
    src: `${IMG_FOLDER}/ozilconseil.png`
  },
  {
    name: 'Mie Câline',
    url: 'https://www.lamiecaline.com/fr/',
    src: `${IMG_FOLDER}/la-mie-caline.png`
  },
  {
    name: 'B1ChezSoi',
    url: 'http://b1chezsoi.com/',
    src: `${IMG_FOLDER}/b1chezsoi.png`
  },
  {
    name: 'Orange',
    url: 'https://www.orange.fr/portail',
    src: `${IMG_FOLDER}/orange.png`
  },
  {
    name: 'Cereg',
    url: 'https://cereg.com/',
    src: `${IMG_FOLDER}/cereg.png`
  },
  {
    name: "Caisse d'Epargne",
    url: 'https://www.caisse-epargne.fr',
    src: `${IMG_FOLDER}/caisse-epargne.png`
  },
  {
    name: 'Enedis',
    url: 'https://www.enedis.fr/',
    src: `${IMG_FOLDER}/enedis.png`
  },
  {
    name: 'SDTech',
    url: 'https://groupe.sd-tech.com/',
    src: `${IMG_FOLDER}/sdtech.png`
  },
  {
    name: 'Cévennes Audition',
    url:
      'https://www.ales-commerces.com/commerces/optique-photo-audition/cevennes-audition-451.html',
    src: `${IMG_FOLDER}/cevennes-audition.png`
  },
  {
    name: 'Edipoles',
    url: 'https://www.edipoles.com/',
    src: `${IMG_FOLDER}/edipoles.png`
  },
  {
    name: 'Point S',
    url: 'https://www.points.fr/',
    src: `${IMG_FOLDER}/points.png`
  }
]

function Mecenes ({ spaced }: MecenesProps): JSX.Element {
  return (
    <div className={_.Mecenes}>
      <Section
        light={false}
        spaced={spaced}
        wave={'/static/images/arrow-bottom.svg'}
        reverseWave
      >
        <>
          <Headline tag='h1'>Nos mécènes</Headline>
          <ImageGrid items={data} />
          <div className={_.Infos}>
            <div className={_.InfosText}>
              <h3>le mecenat c’est quoi ?</h3>
              <Fade>
                <Text align='justify'>
                  Le Ministère de la Santé, de la Jeunesse, des Sports et de la
                  Vie associative décrit le mécénat comme « un soutien matériel
                  apporté sans contrepartie directe de la part du bénéficiaire à
                  une œuvre ou à une personne pour l’exercice d’activités
                  présentant un intérêt général », s’étendant à divers champs
                  dont la culture, le sport, la solidarité, l’environnement.
                </Text>
              </Fade>
              <Fade delay={100}>
                <Text align='justify'>
                  Souvent associé au parrainage, le mécénat s’en distingue sous
                  un angle essentiel : celui de la contrepartie. En effet, le
                  mécène ne recherche pas de contrepartie (sauf éventuellement
                  la citation de son nom) quand le parrain s’engage avec le
                  bénéficiaire dans une opération de nature commerciale en vue
                  d’en retirer un bénéfice direct.
                </Text>
              </Fade>
              <Fade delay={200}>
                <Text align='justify'>
                  Pour autant, pour l’entreprise, le mécénat reste une
                  opportunité : il offre la possibilité d’être reconnu comme un
                  interlocuteur à part entière de son territoire d’implantation.
                  En s’engageant concrètement dans des projets locaux (ou plus
                  largement citoyens), le mécène s’affirme comme un acteur
                  social et responsable, tout en contribuant à renforcer
                  l’attractivité économique de son territoire.
                </Text>
                <Text align='justify'>
                  Au-delà de l’aspect de l’image, le mécénat est aussi un
                  excellent prétexte pour l’entrepreneur de rencontrer ses
                  interlocuteurs habituels dans un contexte différent et riche
                  d’échanges, tout en élargissant son réseau.
                </Text>
              </Fade>
            </div>
            <div className={_.InfosImage}>
              <Fade bottom delay={300}>
                <img src='/static/images/mecenes.svg' />
              </Fade>
            </div>
          </div>
        </>
      </Section>
    </div>
  )
}

export default Mecenes
