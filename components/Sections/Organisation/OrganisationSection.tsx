import React from 'react'
import { useRouter } from 'next/router'

import _ from './OrganisationSection.scss'
import Headline from '../../UI/Headline/Headline'
import Section from '../../UI/Section/Section'
import MiniPost from '../../MiniPost/MiniPost'

import Zoom from 'react-reveal/Zoom'
import { createNavigate } from '../../../helpers'

type OrganisationSectionProps = {}

const ANIMATION_STEP = 500
const ANIMATION_START = 100
const ANIMATION_POST_OFFSET = 30

function OrganisationSection (props: OrganisationSectionProps): JSX.Element {
  const router = useRouter()
  const navigate = createNavigate(router)
  return (
    <div className={_.OrganisationSection}>
      <Section
        light={false}
        spaced
        style={{ paddingBottom: '160px' }}
        wave='/static/images/wave_top.svg'
      >
        <>
          <Headline tag={'h1'}>Organisation</Headline>
          <div className={_.Arrows}>
            <Zoom delay={ANIMATION_START}>
              <img src='/static/images/orga_arrow_left.svg' />
            </Zoom>
            <Zoom delay={ANIMATION_START + ANIMATION_STEP}>
              <img src='/static/images/orga_arrow_middle.svg' />
            </Zoom>
            <Zoom delay={ANIMATION_START + ANIMATION_STEP * 2}>
              <img src='/static/images/orga_arrow_right.svg' />
            </Zoom>
          </div>
          <div className={_.Grid}>
            <div className={_.Column3}>
              <Zoom delay={ANIMATION_START + ANIMATION_POST_OFFSET}>
                <MiniPost
                  title='Culture'
                  titleTag='h3'
                  image='/static/images/culture_orange.svg'
                  body='Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate veniam quibusdam deserunt velit quod tempore, perspiciatis aspernatur alias molestias omnis modi. Veniam nulla vitae delectus earum quam modi hic quibusdam.'
                  button={{
                    label: 'Comité Culture',
                    href: 'culture',
                    type: 'accent',
                    onClick: () => navigate('/culture')
                  }}
                />
              </Zoom>
            </div>
            <div className={_.Column3}>
              {' '}
              <Zoom
                delay={ANIMATION_START + ANIMATION_STEP + ANIMATION_POST_OFFSET}
              >
                <MiniPost
                  title='Sport'
                  titleTag='h3'
                  image='/static/images/sports_orange.svg'
                  body='Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate veniam quibusdam deserunt velit quod tempore, perspiciatis aspernatur alias molestias omnis modi. Veniam nulla vitae delectus earum quam modi hic quibusdam.'
                  button={{
                    label: 'Comité Sports',
                    href: 'sports',
                    type: 'accent',
                    onClick: () => navigate('/sport')
                  }}
                />
              </Zoom>
            </div>
            <div className={_.Column3}>
              {' '}
              <Zoom
                delay={
                  ANIMATION_START + ANIMATION_POST_OFFSET + ANIMATION_STEP * 2
                }
              >
                <MiniPost
                  title='Administration'
                  titleTag='h3'
                  image='/static/images/administration_orange.svg'
                  body='Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate veniam quibusdam deserunt velit quod tempore, perspiciatis aspernatur alias molestias omnis modi. Veniam nulla vitae delectus earum quam modi hic quibusdam.'
                  button={{
                    label: "Conseil d'administration",
                    href: 'administration',
                    type: 'accent',
                    onClick: () => navigate('/administration')
                  }}
                />
              </Zoom>
            </div>
          </div>
        </>
      </Section>
    </div>
  )
}

export default OrganisationSection
