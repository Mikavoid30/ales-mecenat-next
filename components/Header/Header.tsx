import React, { useState } from 'react'
import style from './Header.module.scss'
import Link from 'next/link'

type HeaderProps = {
  light?: boolean
}

function Header ({ light }: HeaderProps) {
  const [menuOpen, setMenuOpen] = useState(false)

  return (
    <div
      className={`${style.Header} ${
        light ? style.HeaderLight : style.HeaderDark
      }`}
    >
      <div className={style.HeaderContent}>
        <div className={style.Logo}>
          <Link href='/'>
            <a>
              <img src='/static/images/logo.svg' />
            </a>
          </Link>
        </div>
        <div
          className={[style.Burger, menuOpen ? style.Open : ''].join(' ')}
          onClick={() => setMenuOpen(!menuOpen)}
        >
          <span></span>
          <span></span>
          <span></span>
        </div>
        <nav className={[style.Nav, menuOpen ? style.Open : ''].join(' ')}>
          <ul>
            <li>
              <Link href='/'>
                <a>Acceuil</a>
              </Link>
            </li>
            <li>
              <Link href='/organisation'>
                <a>Organisation</a>
              </Link>
            </li>
            <li>
              <Link href='/blog'>
                <a>Blog</a>
              </Link>
            </li>
            <li>
              <Link href='/depot-dossier'>
                <a>Dépôt de dossier</a>
              </Link>
            </li>
            {/* <li>
              <Link href='/contact'>
                <a>Contact</a>
              </Link>
            </li> */}
          </ul>
        </nav>
      </div>
    </div>
  )
}

export default Header
