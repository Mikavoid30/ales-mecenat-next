import React from 'react'

import _ from './BlogPostPreview.scss'
import Button from '../../UI/Button/Button'
import { navigate } from '../../../helpers'

type BlogPostPreviewProps = {
  id: string
  title: string
  date: Date
  excerpt: string
  slug: string
  imageURL: string
  size?: 'S' | 'M'
  scheme?: 'Dark' | 'Light'
  reversed?: boolean
  noShadow?: boolean
}

function BlogPostPreview ({
  title,
  date,
  excerpt,
  slug,
  imageURL,
  size = 'S',
  reversed = false,
  noShadow = false,
  scheme = 'Light'
}: BlogPostPreviewProps): JSX.Element {
  return (
    <div
      className={`${_.BlogPostPreview} ${_[`BlogPostPreviewSize${size}`]} ${
        reversed ? _.BlogPostPreviewReversed : ''
      } ${noShadow ? _.NoShadow : ''} ${_[`Scheme${scheme}`]}`}
    >
      <div className={_.ImageContainer}>
        <img src={imageURL} />
      </div>
      <div className={_.BodyContainer}>
        <h4 dangerouslySetInnerHTML={{ __html: title }}></h4>
        <div className={_.Meta}>
          <span>{date.toLocaleDateString()} </span>
        </div>
        <div className={_.Body} dangerouslySetInnerHTML={{ __html: excerpt }} />
        <div className={_.Button}>
          <Button
            size='S'
            accent
            onClick={() => {
              navigate(`/blog/post/${slug}`)
            }}
          >
            Lire l'article
          </Button>
        </div>
      </div>
    </div>
  )
}

export default BlogPostPreview
