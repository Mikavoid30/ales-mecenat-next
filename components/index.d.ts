declare module '*.css'
declare module '*.scss'
declare module '*.png'
declare module '*.jpg'
declare module '*.svg'
declare module 'react-reveal/Fade'
declare module 'react-reveal/Swing'
declare module 'react-reveal/Zoom'

declare type PostPreview = {
  id: string
  title: string
  date: Date
  excerpt: string
  slug: string
  imageURL: string
  size?: 'S' | 'M'
}

declare type WP_POST = {
  id: number
  date: string
  slug: string
  status: string
  type: string
  title: { rendered: string }
  content: { rendered: string }
  excerpt: { rendered: string }
  author: number
  featured_media: number
  _embedded: {
    author: {
      name: string
    }
    'wp:featuredmedia': {
      link: string
      media_details: {
        sizes: {
          medium: {
            width: number
            height: number
            source_url: string
          }
          thumbnail: {
            width: number
            height: number
            source_url: string
          }
          medium_large: {
            width: number
            height: number
            source_url: string
          }
          full: {
            width: number
            height: number
            source_url: string
          }
        }
      }
    }[]
  }
}
