import React from 'react'
import Head from 'next/head'
import '../../style/general.scss'

import style from './Layout.scss'
import FooterSection from '../Sections/Footer/FooterSection'

type LayoutProps = {
  children: JSX.Element[]
}

function Layout ({ children }: LayoutProps) {
  return (
    <>
      <Head>
        <style global>
          {`
            @font-face {
              font-family: 'Vibur';
              src: url('./static/fonts/Vibur/Vibur-Regular.ttf');
            }
            @font-face {
              font-family: 'Roboto';
              src: url('./static/fonts/Roboto/Roboto-Regular.ttf');
            }

          `}
        </style>
      </Head>
      <div className={style.Layout}>{children}</div>
      <FooterSection />
    </>
  )
}

export default Layout
