import React from 'react'

import _ from './ImageGrid.scss'
import Card from '../UI/Card/Card'
import Zoom from 'react-reveal/Zoom'

type ImageGridProps = {
  items: { url: string; src: string; name: string }[]
  size?: number
}

function ImageGrid ({ items, size = 5 }: ImageGridProps): JSX.Element {
  return (
    <div className={_.ImageGrid}>
      <div className={_.Grid}>
        <div className={_.Row} key={'row'}>
          {items.map((item, id) => (
            <div
              className={_.Item}
              key={item.name}
              style={{ width: `${Math.floor(100 / size)}%` }}
            >
              <Zoom delay={id * 100}>
                <Card img={item.src} url={item.url} id={id} />
              </Zoom>
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}

export default ImageGrid
