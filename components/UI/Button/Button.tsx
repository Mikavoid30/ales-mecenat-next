import React from 'react'

import style from './Button.scss'

type ButtonProps = {
  onClick?: () => any
  type?: 'button' | 'submit' | 'reset' | undefined
  primary?: boolean
  accent?: boolean
  size?: 'S' | 'M' | 'L'
  children?: any
  white?: boolean
}

function Button ({
  children,
  primary,
  accent,
  type,
  white,
  onClick,
  size = 'S'
}: ButtonProps) {
  return (
    <button
      type={type}
      className={`${style.Button}
        ${primary ? style.Primary : ''}
        ${accent ? style.Accent : ''}
        ${white ? style.White : ''}
        ${style[size || 'S']}
        `}
      onClick={onClick}
    >
      {children}
    </button>
  )
}

export default Button
