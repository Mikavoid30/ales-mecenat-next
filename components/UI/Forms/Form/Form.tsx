import React from 'react'

import _ from './Form.scss'
import Button from '../../Button/Button'

type FormProps = {
  onSubmit: (...args: any[]) => any
  submitLabel?: string
  children?: JSX.Element | JSX.Element[] | string
}

function Form ({
  submitLabel = 'Submit',
  onSubmit,
  children
}: FormProps): JSX.Element {
  return (
    <form
      className={_.Form}
      onSubmit={(e) => {
        e.preventDefault()
        onSubmit()
      }}
    >
      {children}
      <Button type={'submit'} white>
        {submitLabel}
      </Button>
    </form>
  )
}

export default Form
