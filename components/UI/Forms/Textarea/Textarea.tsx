import React from 'react'

import _ from './Textarea.scss'

type TextareaProps = {
  value: string
  placeholder: string
  name: string
  required?: boolean
  onChange?: (e: any) => any
  children?: string
}

function Textarea ({
  value,
  placeholder,
  name,
  required,
  children,
  onChange
}: TextareaProps): JSX.Element {
  return (
    <textarea
      className={_.Textarea}
      name={name}
      required={required}
      placeholder={placeholder}
      onChange={onChange}
    >
      {children}
    </textarea>
  )
}

export default Textarea
