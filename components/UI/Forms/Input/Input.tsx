import React from 'react'

import _ from './Input.scss'

type InputProps = {
  type: 'text' | 'email' | 'password'
  value: string
  placeholder: string
  name: string
  required?: boolean
  onChange?: (e: any) => any
  children?: string
}

function Input ({
  type,
  value,
  placeholder,
  name,
  required,
  children,
  onChange
}: InputProps): JSX.Element {
  return (
    <input
      className={_.Input}
      name={name}
      type={type}
      value={value}
      required={required}
      placeholder={placeholder}
      onChange={onChange}
    >
      {children}
    </input>
  )
}

export default Input
