import React from 'react'

import _ from './Card.scss'

type CardProps = {
  title?: string
  img?: string
  url?: string
  id?: number
}

function getTag (link = '') {
  if (link) return 'a'
  return 'div'
}

function Card ({ title, img, url, id }: CardProps): JSX.Element {
  const Tag = getTag(url)
  const linkProps = url
    ? {
        target: '_blank',
        href: url
      }
    : {}
  return (
    <div className={_.Card}>
      <Tag {...linkProps}>
        {title && <h1>{title}</h1>}
        {img && <img src={img} />}
      </Tag>
    </div>
  )
}

export default Card
