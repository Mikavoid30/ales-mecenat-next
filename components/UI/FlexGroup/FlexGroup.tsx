import React from 'react'

import style from './FlexGroup.scss'

type FlexGroupProps = {
  children: any
  align: 'left' | 'center' | 'right'
  vertical?: boolean
}
function FlexGroup({
  children,
  align = 'left',
  vertical = false
}: FlexGroupProps) {
  return (
    <div
      className={`
      ${style.FlexGroup}
      ${vertical ? style.Vertical : style.Horizontal}
      ${style[align]}
    `}
    >
      {children}
    </div>
  )
}

export default FlexGroup
