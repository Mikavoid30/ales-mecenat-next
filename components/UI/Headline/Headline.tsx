import React from 'react'
import Fade from 'react-reveal/Fade'
import style from './Headline.scss'

type HeadlineProps = {
  children: any
  primary?: boolean
  tag: any
  noPadding?: boolean
}

function Headline ({
  children,
  primary,
  noPadding,
  tag = 'h1'
}: HeadlineProps) {
  const Tag = tag
  return (
    <div
      className={`${style.Headline} ${
        primary ? style.Primary : style.Standard
      } ${noPadding ? style.NoPadding : style.Padding}`}
    >
      <Fade>
        <Tag className={style.Title}>{children}</Tag>
      </Fade>
    </div>
  )
}

export default Headline
