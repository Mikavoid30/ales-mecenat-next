import React from 'react'

import _ from './Section.scss'
import { reverse } from 'dns'

type SectionProps = {
  light?: boolean
  wave?: string
  wavePosition?: number
  fluid?: boolean
  spaced?: boolean
  reverseWave?: boolean
  style?: {}
  children?: JSX.Element | JSX.Element[] | undefined
}

function Section ({
  light,
  fluid,
  wave,
  spaced,
  reverseWave,
  style,
  wavePosition = -2,
  children
}: SectionProps): JSX.Element {
  return (
    <div
      className={`${_.Section} ${light ? _.SectionLight : _.SectionDark} ${
        spaced ? _.Spaced : ''
      }`}
      style={style}
    >
      <div className={`${_.Container} ${fluid ? _.Fluid : ''}`}>{children}</div>
      {wave && (
        <div
          className={`${_.WaveContainer} ${light ? _.WaveLight : _.WaveDark}`}
        >
          <img
            className={`${_.Wave} ${light ? _.WaveLight : _.WaveDark} ${
              reverseWave ? _.Reverse : ''
            }`}
            src={wave}
          />
        </div>
      )}
    </div>
  )
}

export default Section
