import React from 'react'

import _ from './FlexGrid.scss'
import Zoom from 'react-reveal/Zoom'

type FlexGridProps = {
  children: JSX.Element[]
  size?: number
}

function FlexGrid ({ size = 4, children }: FlexGridProps): JSX.Element {
  return (
    <div className={_.FlexGrid}>
      <div className={_.Grid}>
        {children.map((child: JSX.Element) => {
          return (
            <div
              className={_.Row}
              style={{ width: `${Math.floor(100 / size)}%` }}
            >
              {child}{' '}
            </div>
          )
        })}
      </div>
    </div>
  )
}

export default FlexGrid
