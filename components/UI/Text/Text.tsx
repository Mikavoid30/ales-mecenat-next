import React from 'react'

import _ from './Text.scss'

type TextProps = {
  align?: 'center' | 'right' | 'left' | 'justify'
  width?: number
  style?: {}
  children: string
}

function Text ({
  children,
  width = 100,
  align = 'left',
  style = {}
}: TextProps): JSX.Element {
  const s = {
    textAlign: align,
    width: `${width}%`,
    ...style
  }
  return (
    <p className={_.Text} style={s}>
      {children}
    </p>
  )
}

export default Text
