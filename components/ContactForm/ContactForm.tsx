import React, { useState } from 'react'

import _ from './ContactForm.scss'
import Form from '../UI/Forms/Form/Form'
import Input from '../UI/Forms/Input/Input'
import Textarea from '../UI/Forms/Textarea/Textarea'

type ContactFormProps = {}

function onSubmit (values: any) {
  console.log({ values })
}

function ContactForm (props: ContactFormProps): JSX.Element {
  const [values, setValues] = useState({
    name: '',
    email: '',
    message: ''
  })
  return (
    <div className={_.ContactForm}>
      <Form submitLabel='Envoyer' onSubmit={() => onSubmit(values)}>
        <Input
          name='name'
          type='text'
          placeholder={'Votre nom'}
          required
          value={values.name}
          onChange={(e: any) => setValues({ ...values, name: e.target.value })}
        />
        <Input
          name='email'
          type='email'
          placeholder={'Adresse email'}
          required
          value={values.email}
          onChange={(e: any) => setValues({ ...values, email: e.target.value })}
        />
        <Textarea
          name='message'
          placeholder={'Message'}
          required
          value={values.message}
          onChange={(e: any) =>
            setValues({ ...values, message: e.target.value })
          }
        />
      </Form>
    </div>
  )
}

export default ContactForm
