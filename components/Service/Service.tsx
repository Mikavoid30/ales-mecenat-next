import React from 'react'

import style from './Service.scss'
import Text from '../UI/Text/Text'
import Fade from 'react-reveal/Fade'

type ServiceProps = {
  image: any
  imagePosition: 'left' | 'right'
  data: { title: string; text: string }
  children: any
}

function Service ({ image, imagePosition, children, data }: ServiceProps) {
  const fadeFrom: any = {}
  fadeFrom[imagePosition] = true
  return (
    <div className={`${style.Service}`}>
      <div
        className={`${style.ServiceContainer} ${
          imagePosition === 'left' ? style.LTR : style.RTL
        }`}
      >
        <Fade {...fadeFrom}>
          <div className={style.ImageContainer}>
            <img src={image} />
          </div>
          <div className={style.Content}>
            <h2 className={style.ServiceTitle}>{data.title}</h2>
            <Text>{data.text}</Text>
            {children}
          </div>
        </Fade>
      </div>
    </div>
  )
}

export default Service
