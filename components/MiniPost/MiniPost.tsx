import React from 'react'

import _ from './MiniPost.scss'
import Headline from '../UI/Headline/Headline'
import Text from '../UI/Text/Text'
import Button from '../UI/Button/Button'

type MiniPostProps = {
  title?: string
  titleTag?: string
  body?: string
  image?: string
  button?: {
    label: string
    href?: string
    type?: 'accent' | 'primary'
    onClick?: () => any
  }
}

function MiniPost ({
  title,
  titleTag = 'h1',
  body,
  image,
  button
}: MiniPostProps): JSX.Element {
  const buttonType: any = {}
  const TitleTag: any = titleTag
  if (button && button.type) buttonType[button.type] = true
  return (
    <div className={_.MiniPost}>
      {title && <TitleTag>{title}</TitleTag>}
      {image && <img src={image} />}
      {body && <Text>{body}</Text>}
      {button && (
        <Button {...buttonType} onClick={button.onClick}>
          {button.label}
        </Button>
      )}
    </div>
  )
}

export default MiniPost
